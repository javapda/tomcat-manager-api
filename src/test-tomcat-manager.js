const TomcatManager = require('./tomcat-manager')

new TomcatManager()
.serverUrl('http://my-tomcat-server:8080')
.username('my-username')
.password('my-password')
.listApps()
.then((tomcatInfo)=>{
    console.log(`tomcatInfo=${JSON.stringify(tomcatInfo)}`); 
    console.log(`command status: ${tomcatInfo.commandStatus}`)
    console.log(`No. apps: ${tomcatInfo.apps.length}`)
})
.catch((err)=>console.log(`err=${err}`))
