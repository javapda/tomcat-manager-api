const axios = require('axios')
class TomcatApp {
    constructor(tomcatAppString) {
        const pieces = tomcatAppString.split(":")
        this.context = pieces[0]
        this.state = pieces[1]
        this.num = pieces[2]
        this.name= pieces[3]
    }
    context() {return this.context}
    state() {return this.state}
    num() {return this.num}
    name() {return this.name}
    toString() {
        return `context=${context}, name=${name}, num=${num}, state=${state}`
    }
}
module.exports = class TomcatManager {
    constructor() {
        this._serverUrl = 'never-set'
    }
    serverUrl(url) {
        this._serverUrl = url
        return this
    }
    username(username) {
        if (!this.auth) {
            this.auth = {}
        }
        this.auth.username=username
        return this
    }
    password(password) {
        if (!this.auth) {
            this.auth = {}
        }
        this.auth.password=password
        return this
    }

    listApps() {
        return new Promise((resolve, reject)=>{
            const url = `${this._serverUrl}/manager/text/list`
            const props = {}
            if (this.auth) {
                props.auth=this.auth
            }
            axios.get(url, props)
            .then((result)=>{
                const items=result.data.split("\n")
                //console.log(`length=${items.length}, items:${items}`)
                const apps=[]
                const tomcatInfo = {apps:apps, commandUrl:url}
                for (var i=0; i< items.length;i++) {
                    const item=items[i]
                    if (item.split(":").length===4) {
                        apps.push(new TomcatApp(item))
                    } else {
                        if (item.trim().length!==0) {
                            tomcatInfo.commandStatus = item
                        } 
                    }
                }
                resolve(tomcatInfo)
            }
            )
            .catch((err)=>reject(err))
        });
    }
}