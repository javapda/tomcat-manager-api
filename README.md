# tomcat-manager-api #

* Promise-based interface to tomcat manager

# Installation / setup #
```
npm install tomcat-manager-api --save

```

# Usage #

## from node ##
```
const {TomcatManager} = require('tomcat-manager-api')

new TomcatManager()
    .serverUrl(`http://my-tomcat-server:8080`)
    .username('myusername')
    .password('mypassword')
    .listApps()
    .then((tomcatInfo)=>{
        console.log(`tomcatInfo=${JSON.stringify(tomcatInfo)}`); 
        console.log(`command status: ${tomcatInfo.commandStatus}`)
        console.log(`No. apps: ${tomcatInfo.apps.length}`)
    })
    .catch((err)=>console.log(`problem, err=${err}))
```

## output ##
```
tomcatInfo={
  "apps": [
    {
      "context": "/",
      "state": "running",
      "num": "0",
      "name": "ROOT"
    },
    {
      "context": "/examples",
      "state": "running",
      "num": "0",
      "name": "examples"
    },
    {
      "context": "/host-manager",
      "state": "running",
      "num": "0",
      "name": "host-manager"
    },
    {
      "context": "/manager",
      "state": "running",
      "num": "1",
      "name": "manager"
    },
    {
      "context": "/docs",
      "state": "running",
      "num": "0",
      "name": "docs"
    }
  ],
  "commandUrl": "http://my-tomcat-server:8080/manager/text/list",
  "commandStatus": "OK - Listed applications for virtual host [localhost]"
}
command status: OK - Listed applications for virtual host [localhost]
No. apps: 5
```

# Resources #

* [tomcat 8.5 manager HOW-TO](https://tomcat.apache.org/tomcat-8.5-doc/manager-howto.html)
* [axios](https://www.npmjs.com/package/axios)